import pandas as pd
from xgboost import XGBClassifier
import joblib
import os
from utils import sex_insured_mapping, vehicle_type_mapping

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.pipeline import Pipeline


if __name__=="__main__":

    dataframe = pd.read_csv(os.path.join(os.getcwd(), "data", "SingaporeAuto.csv"), sep=",")

    label = dataframe['Clm_Count']
    data = dataframe.drop(['Clm_Count'], axis=1)
    data['SexInsured'] = data["SexInsured"].map(sex_insured_mapping)
    data['VehicleType'] = data["VehicleType"].map(vehicle_type_mapping)


    data_train, data_test, label_train, label_test = train_test_split(data, label, test_size=0.2, random_state=30, shuffle=False)



    scaler = StandardScaler()
    pca = PCA()
    classifier = XGBClassifier()

    pipeline = Pipeline([
            ('standard_scaler', scaler),
            ('pca', pca),
            ('model', classifier)
            ])

    pipeline.fit(data_train, label_train)

    print(f"Score -> {pipeline.score(data_test, label_test)}")

    joblib.dump(pipeline, os.path.join(os.getcwd(), "models", "SingaporeAuto_Model.pkl"))
