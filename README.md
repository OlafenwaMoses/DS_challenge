# DS Challenge for Monitaur

## Goal
Build a basic classification model using a Scikit Learn pipeline and deploy via a REST API

## Format Requirements
- Fork or clone this repo
- Maintain the directory structure given for the project
- Use Python 3.7+
- If you need additional imports specify them in `requirements.txt`

## Model Requirements
- Model should be in the form of a [scikit-learn pipeline](https://scikit-learn.org/stable/modules/generated/sklearn.pipeline.Pipeline.html)
- Create a model artifact using Pickle or Joblib

<!-- - Achieve a model RMSE less than or equal to 2.551 against some withheld test set -->

## API Requirements
- Serve the model as a REST API using Flask or FastAPI
- Be able to use CURL to send in feature inputs and return the prediction.
<!-- - Output must conform to the format given in the `Test Case` -->

## Data
The dataset you will be using contains information about Singapore Automobile Claims. You will be predicting the `Clm_Count`, you may generate non-Integer predictions.

[Source](https://instruction.bus.wisc.edu/jfrees/jfreesbooks/Regression%20Modeling/BookWebDec2010/CSVData/SingaporeAuto.csv)

[Format](https://rdrr.io/cran/insuranceData/man/SingaporeAuto.html)

[Data Description (see Page 21)](https://instruction.bus.wisc.edu/jfrees/jfreesbooks/Regression%20Modeling/BookWebDec2010/DataDescriptions.pdf)

<!-- ## Test Case
Run the following curl command against your model to verify that your model works as we expect:

```
TODO: Provide CURL command and output format
``` -->

## Submission
Timebox this challenge to 3 hours. After completing the assignment, you will meet with the data science team to demo your results and share your code.


# Usage

Install dependencies with the command

```
pip install -r requirements.txt
```

## Train Model
The claffication model used in this project is an XGBClassifier. To train the model, run the command below

```
python model_trainer.py
```

The `model_trainer.py` file will
- read the `SingaporeAuto.csv` dataset as a dataframe from `data` folder
- Map non-numerical columns to numeric values and split the data to train and test sets
- create a `sklearn Pipeline` using XGBClassifier
- train the model and train set
- Compute the accuracy/score of the model on the test set
- Save the output model as a `.pkl` file to the `models` folder.


## Run Flask App
To serve the saved model as a classifiction API, run the command below

```
python app.py
```

## Test the API

Find below a sample **CURL** command for sending a classification `POST` request

```
curl -d "sexInsured=U&female=false&vehicleType=T&expWeights=0.668035592&lnWeight=-0.403413825&ncd=30&ageCat=0&autoAge0=0&autoAge1=0&autoAge2=0&autoAge=0&vAgeCat=0&vAgeCat1=2&pc=0" -X POST http://127.0.0.1:5000/classify
```

Accepted parameters are:
- sexInsured (string)
- female (boolean)
- vehicleType (string)
- expWeights (float)
- lnWeight (float)
- ncd (integer)
- pc (integer)
- ageCat (integer)
- autoAge (integer)
- autoAge0 (integer)
- autoAge1 (integer)
- autoAge2 (integer)
- vAgeCat (integer)
- vAgecat1 (integer)

