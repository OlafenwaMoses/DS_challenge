# Hosts the REST API
from flask import Flask, request, jsonify
from model import classify

app = Flask(__name__)

classify_route = "/classify"


@app.route(classify_route, methods=["POST"])
def classify_api():
    sex_insured = request.form.get("sexInsured")
    female = request.form.get("female")
    vehicle_type = request.form.get("vehicleType")
    pc = request.form.get("pc")
    exp_weights = request.form.get("expWeights")
    ln_weight = request.form.get("lnWeight")
    ncd = request.form.get("ncd")
    age_cat = request.form.get("ageCat")
    auto_age0 = request.form.get("autoAge0")
    auto_age1 = request.form.get("autoAge1")
    auto_age2 = request.form.get("autoAge2")
    auto_age = request.form.get("autoAge")
    v_age_cat = request.form.get("vAgeCat")
    v_age_cat1 = request.form.get("vAgeCat1")

    classification = classify(
        sex_insured,
        female,
        vehicle_type,
        pc,
        exp_weights,
        ln_weight,
        ncd,
        age_cat,
        auto_age,
        auto_age0, 
        auto_age1,
        auto_age2,
        v_age_cat,
        v_age_cat1
    )

    data = {'Clm_Count': int(classification)}
    return jsonify(data), 200



if __name__=="__main__":
    app.run()
