import joblib
import os
import pandas as pd
from utils import sex_insured_mapping, vehicle_type_mapping, female_mapping


SingaporeAuto_model = joblib.load(os.path.join(os.getcwd(), "models", "SingaporeAuto_Model.pkl"))

def classify(
    sex_insured: str,
    female:str,
    vehicle_type:str,
    pc:int,
    exp_weights: float,
    ln_weight: float,
    ncd: int,
    age_cat: int,
    auto_age: int,
    auto_age0: int,
    auto_age1: int,
    auto_age2: int,
    v_age_cat: int,
    v_age_cat1: int
):



    inputs = pd.DataFrame([[
        sex_insured_mapping[sex_insured],
        female_mapping[female],
        vehicle_type_mapping[vehicle_type],
        pc,
        exp_weights,
        ln_weight,
        ncd,
        age_cat,
        auto_age0,
        auto_age1,
        auto_age2,
        auto_age,
        v_age_cat,
        v_age_cat1
        ]],
        
        columns=[
            "SexInsured",
            "Female",
            "VehicleType",
            "PC",
            "Exp_weights",
            "LNWEIGHT",
            "NCD",
            "AgeCat",
            "AutoAge0",
            "AutoAge1",
            "AutoAge2",
            "AutoAge",
            "VAgeCat",
            "VAgecat1"])
    
    return SingaporeAuto_model.predict(inputs)[0]
